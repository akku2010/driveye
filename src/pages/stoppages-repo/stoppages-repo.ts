import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-stoppages-repo',
  templateUrl: 'stoppages-repo.html',
})
export class StoppagesRepoPage implements OnInit {

  stoppagesReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices: any;
  portstemp: any = [];
  Stoppagesdata: any;
  datetime: number;
  selectedVehicle: any;
  vehicleData: any;
  locationEndAddress: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallStoppages: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
    }
  }

  ngOnInit() {
    if (this.vehicleData == undefined) {
      this.getdevices();
    } else {
      this.Ignitiondevice_id = this.vehicleData._id;
      this.getStoppageReport();
    }
  }

  getdevices() {
    var baseURLp = this.apicallStoppages.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicallStoppages.startLoading().present();
    this.apicallStoppages.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicallStoppages.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicallStoppages.stopLoading();
          console.log(err);
        });
  }


  getStoppagesdevice(data) {
    console.log("selectedVehicle=> ", data)
    this.Ignitiondevice_id = data._id;
  }

  getStoppageReport() {
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";
    }
    let that = this;
    this.apicallStoppages.startLoading().present();
    this.apicallStoppages.getStoppageApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicallStoppages.stopLoading();
        this.stoppagesReport = data;
        this.Stoppagesdata = [];
        if (this.stoppagesReport.length > 0) {
          this.innerFunc(this.stoppagesReport);
        } else {
          let toast = this.toastCtrl.create({
            message: "Report(s) not found for selected dates/vehicle.",
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicallStoppages.stopLoading();
        console.log(error);
      })
  }

  innerFunc(stoppagesReport) {
    let outerthis = this;
    outerthis.locationEndAddress = undefined;
    var i = 0, howManyTimes = stoppagesReport.length;
    function f() {
      var fd = new Date(outerthis.stoppagesReport[i].arrival_time).getTime();
      var td = new Date(outerthis.stoppagesReport[i].departure_time).getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      var Durations = rhours + 'hrs : ' + rminutes + 'mins';

      outerthis.Stoppagesdata.push({
        'arrival_time': outerthis.stoppagesReport[i].arrival_time,
        'departure_time': outerthis.stoppagesReport[i].departure_time,
        'Durations': Durations,
        'device': outerthis.stoppagesReport[i].device ? outerthis.stoppagesReport[i].device.Device_Name : 'N/A',
        'end_location': {
          'lat': outerthis.stoppagesReport[i].lat,
          'long': outerthis.stoppagesReport[i].long
        }
      });

      outerthis.end_address(outerthis.Stoppagesdata[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  end_address(item, index) {
    let that = this;
    that.Stoppagesdata[index].EndLocation = "N/A";
    if (!item.end_location) {
      that.Stoppagesdata[index].EndLocation = "N/A";
    } else if (item.end_location) {
      this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
        .then((res) => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
          that.Stoppagesdata[index].EndLocation = str;
        })
    }
  }
  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicallStoppages.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }
}
