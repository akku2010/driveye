import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-your-trips',
  templateUrl: 'your-trips.html',
})
export class YourTripsPage {
  islogin: any;
  datetimeStart: any;
  datetimeEnd: any;
  portstemp: any[] = [];
  trip_id: any;
  _tripList: any[] = [];
  startAdd: any;
  endAdd: any;
  addkey1: any;
  addkey2: any;
  skip: number = 1;
  limit: number = 10;
  _tripListKey: any[] = [];
  selectedVehicle: any;

  constructor(
    public apicall: ApiServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    this.getdevices();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YourTripsPage');
  }

  getdevices() {
    var baseURLp = this.apicall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicall.startLoading().present();
    this.apicall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apicall.stopLoading();
          console.log(err);
        });
  }

  getID(val) {
    this.trip_id = val._id;
  }

  getTrips(infiniteScroll) {
    var url;
    let that = this;

    if (!infiniteScroll) {
      this._tripListKey = [];
      this._tripList = [];
    }

    if (this.trip_id != undefined) {
      url = this.apicall.mainUrl + "user_trip/getLastTrip?device=" + this.trip_id + "&tripInfo=all_trip&user=" + this.islogin._id + "&fromDate=" + new Date(this.datetimeStart).toISOString() + "&toDate=" + new Date(this.datetimeEnd).toISOString() + "&skip=" + that.skip + "&limit=" + that.limit;
    } else {
      url = this.apicall.mainUrl + "user_trip/getLastTrip?&tripInfo=all_trip&user=" + this.islogin._id + "&fromDate=" + new Date(this.datetimeStart).toISOString() + "&toDate=" + new Date(this.datetimeEnd).toISOString() + "&skip=" + that.skip + "&limit=" + that.limit;
    }
    if (!infiniteScroll) {
      this.apicall.startLoading().present();
      this.apicall.getSOSReportAPI(url)
        .subscribe(data => {
          this.apicall.stopLoading();
          console.log("trip data: ", data)

          if (!data.message) {
            this.innerFunc(data, infiniteScroll);
          } else {
            let toast = this.toastCtrl.create({
              message: "Trip(s) not found..!",
              duration: 1500,
              position: "bottom"
            })
            toast.present();
          }

        },
          err => {
            this.apicall.stopLoading();
          })
    } else {
      this.apicall.getSOSReportAPI(url)
        .subscribe(data => {
          console.log("trip data: ", data)
          debugger
          if (!data.message) {
            this.innerFunc(data, infiniteScroll);
          }
        })
    }
  }

  innerFunc(data, infiniteScroll) {
    let outerthis = this;
    outerthis.startAdd = undefined;
    outerthis.endAdd = undefined;
    var i = 0, howManyTimes = data.length;
    function f() {
      var date1 = moment(new Date(data[i].start_time), 'DD/MM/YYYY').format("LLLL");
      var str = date1.split(', ');
      var date2 = str[0];
      var date3 = str[1].split(' ');
      var date4 = str[2].split(' ');
      var date5 = date4[0];
      var date6 = date3[0];
      var date7 = date3[1];
      var date8 = date6 + ' ' + date5;
      var date9 = date4[1];
      outerthis._tripList.push({
        "date1": date2,
        "date2": date8,
        "date3": date7,
        "dtime": date9,
        "trip_name": data[i].trip_name,
        "trip_status": data[i].trip_status,
        "start_location": {
          "lat": data[i].start_lat,
          "long": data[i].start_long
        },
        "end_location": {
          "lat": data[i].end_lat,
          "long": data[i].end_long
        }
      });

      outerthis.start_address(outerthis._tripList[i], i);
      outerthis.end_address(outerthis._tripList[i], i);

      if (infiniteScroll) {
        outerthis._tripListKey.push(outerthis._tripList);
        // infiniteScroll.complete();
      } else {
        // infiniteScroll.complete();
        outerthis._tripListKey = outerthis._tripList;
      }
      console.log(outerthis._tripListKey);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }
  start_address(item, index) {
    let that = this;
    that._tripList[index].StartLocation = "N/A";
    if (!item.start_location) {
      that._tripList[index].StartLocation = "N/A";
    } else if (item.start_location) {
      this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
        .then((res) => {
          console.log("test", res)
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
          that._tripList[index].StartLocation = str;
        })
    }
  }

  end_address(item, index) {
    let that = this;
    that._tripList[index].EndLocation = "N/A";
    if (!item.end_location) {
      that._tripList[index].EndLocation = "N/A";
    } else if (item.end_location) {
      this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
        .then((res) => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
          that._tripList[index].EndLocation = str;
        })
    }
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicall.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.skip = that.skip + 1;
    setTimeout(() => {
      that.getTrips(infiniteScroll)
      infiniteScroll.complete();
    }, 200);
  }

}
