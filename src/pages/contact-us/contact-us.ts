import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { CallNumber } from '@ionic-native/call-number';
import * as moment from 'moment';
import { IonPullUpFooterState } from 'ionic-pullup';

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage implements OnInit {

  contact_data: any = {};
  contactusForm: FormGroup;
  submitAttempt: boolean;
  islogin: any;
  tickets: any[] = [];
  datetimeStart: any;
  datetimeEnd: any;
  showDatePanel: boolean = false;
  footerState: IonPullUpFooterState;
  openNum: number = 0;
  closeNum: number = 0;
  inprogressNum: number = 0;
  tempTickets: any[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public api: ApiServiceProvider,
    public toastCtrl: ToastController,
    private callNumber: CallNumber
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    this.contactusForm = formBuilder.group({
      name: ['', Validators.required],
      mail: ['', Validators.email],
      mobno: ['', Validators.required],
      note: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
  }

  ngOnInit() {
    this.viewTicket();
  }

  call(num) {
    this.callNumber.callNumber(num, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }
  contactUs() {
    this.submitAttempt = true;
    if (this.contactusForm.valid) {

      // this.contact_data = {
      //   "user": this.islogin._id,
      //   // "email": this.contactusForm.value.mail,
      //   "msg": this.contactusForm.value.note,
      //   // "phone": (this.contactusForm.value.mobno).toString(),

      //   // "dealerid": this.islogin.email,
      //   // "dealerName": this.islogin.fn + ' ' + this.islogin.ln
      //   // "dealerid": "gaurav.gupta@adnatesolutions.com",
      //   // "dealerName": "Gaurav Gupta"
      // }
      var payData = {
        "user": this.islogin._id,
        // "email": this.contactusForm.value.mail,
        "msg": this.contactusForm.value.note,
      }
      this.api.startLoading().present();
      // this.api.contactusApi(this.contact_data)
      this.api.createTicketApi(payData)
        .subscribe(data => {
          this.api.stopLoading();
          console.log(data.message)
          if (data.message == 'saved response') {
            let toast = this.toastCtrl.create({
              // message: 'Your request has been submitted successfully. We will get back to you soon.',
              message: 'Your ticket has been submitted successfully. We will get back to you soon.',
              position: 'bottom',
              duration: 3000
            });

            // toast.onDidDismiss(() => {
            //   console.log('Dismissed toast');
            //   // this.contactusForm.reset();
            //   this.navCtrl.setRoot(ContactUsPage);
            // });

            toast.present();
            this.navCtrl.setRoot(ContactUsPage);
          } else {
            let toast = this.toastCtrl.create({
              message: 'Something went wrong. Please try after some time.',
              position: 'bottom',
              duration: 3000
            });

            toast.onDidDismiss(() => {
              console.log('Dismissed toast');
              this.navCtrl.setRoot(ContactUsPage);
            });

            toast.present();
          }

        },
          error => {
            this.api.stopLoading();
            console.log(error);
          });
    }
  }

  addTicket() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

  viewTicket() {
    var _bUrl = "https://www.oneqlik.in/customer_support/getCustomerQuery";
    var payload = {
      "draw": 1,
      "columns": [
        {
          "data": "_id"
        },
        {
          "data": "superAdmin"
        },
        {
          "data": "dealer"
        },
        {
          "data": "message"
        },
        {
          "data": "assigned_to"
        },
        {
          "data": "assigned_to.first_name"
        },
        {
          "data": "assigned_to.last_name"
        },
        {
          "data": "support_status"
        },
        {
          "data": "posted_on"
        },
        {
          "data": "posted_by"
        },
        {
          "data": "posted_by.first_name"
        },
        {
          "data": "posted_by.last_name"
        },
        {
          "data": "posted_by.phone"
        },
        {
          "data": "posted_by.email"
        },
        {
          "data": "role"
        },
        {
          "data": "user"
        }
      ],
      "order": [
        {
          "column": 3,
          "dir": "desc"
        }
      ],
      "start": 0,
      "length": 50,
      "search": {
        "value": "",
        "regex": false
      },
      "op": {},
      "select": [],
      "find": {
        "assigned_to": this.islogin._id,
        "posted_on": {
          "$gte": {
            "_eval": "date",
            "value": new Date(this.datetimeStart).toISOString()
          },
          "$lte": {
            "_eval": "date",
            "value": new Date(this.datetimeEnd).toISOString()
          }
        }
      }
    }
    this.tickets = [];
    this.openNum = 0;
    this.closeNum = 0;
    this.inprogressNum = 0;
    this.api.startLoading().present();
    this.api.urlpasseswithdata(_bUrl, payload)
      .subscribe(data => {
        this.api.stopLoading();
        console.log("tickets: ", data)
        this.tickets = data.data;
        this.tempTickets = data.data;
        for (var i = 0; i < data.data.length; i++) {
          if (data.data[i].support_status == 'OPEN') {
            this.openNum += 1;
          } else if (data.data[i].support_status == 'CLOSE') {
            this.closeNum += 1;
          } else if (data.data[i].support_status == 'IN PROGRESS') {
            this.inprogressNum += 1;
          }
        }
      },
        err => {
          this.api.stopLoading();
          console.log("getting err while getting data: ", err)
        })
  }

  loadContent(key) {
    if (key === 'PROGRESS') {
      this.tickets = [];
      for (var i = 0; i < this.tempTickets.length; i++) {
        if (this.tempTickets[i].support_status === 'IN PROGRESS') {
          this.tickets.push(this.tempTickets[i]);
        }
      }
    } else if (key === 'OPEN') {
      this.tickets = [];
      for (var r = 0; r < this.tempTickets.length; r++) {
        if (this.tempTickets[r].support_status === 'OPEN') {
          this.tickets.push(this.tempTickets[r]);
        }
      }
    } else if (key === 'CLOSE') {
      this.tickets = [];
      for (var v = 0; v < this.tempTickets.length; v++) {
        if (this.tempTickets[v].support_status === 'CLOSE') {
          this.tickets.push(this.tempTickets[v]);
        }
      }
    }
  }

}
