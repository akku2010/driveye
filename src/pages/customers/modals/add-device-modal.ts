import { Component, OnInit } from "@angular/core";
import { NavParams, ViewController, ToastController, AlertController, ModalController, IonicPage } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
// import { GroupModalPage } from "./group-modal/group-modal";
import * as moment from 'moment';

@IonicPage()
@Component({
    selector: 'page-add-devices',
    templateUrl: 'add-device-modal.html'
})

export class AddDeviceModalPage implements OnInit {
    addvehicleForm: FormGroup;
    submitAttempt: boolean;
    customer: any;
    devicesadd: any;
    islogin: any;
    allGroup: any = [];
    // allGroupName: any;
    deviceModel: any;
    selectUser: any;
    allVehicle: any;
    modeldata: any;
    groupstaus: any;
    userdata: any;
    vehType: any;
    groupstaus_id: any;
    devicedetails: any = {};
    TypeOf_Device: any;
    driverList: any;

    minDate: any;
    currentYear: any;
    modeldata_id: any;
    rootLogin: any;
    constructor(
        public params: NavParams,
        public viewCtrl: ViewController,
        public formBuilder: FormBuilder,
        public apiCall: ApiServiceProvider,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController,
        public modalCtrl: ModalController
    ) {

        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);

        this.currentYear = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log(this.currentYear);

        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one one later date=> " + moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"))
        // =============== end
        this.addvehicleForm = formBuilder.group({
            device_name: ['', Validators.required],
            device_id: ['', Validators.required],
            driver: [''],
            contact_num: ['', Validators],
            sim_number: ['', Validators.required],
            ExipreDate: [this.currentYear, Validators.required],
            device_type: ['', Validators.required],
            name: [''],
            first_name: [''],
            brand: ['', Validators.required],
            fastag: ['']
        });
        if (params.get("custDet")) {
            this.islogin = params.get("custDet");
            console.log("custDel=> ", this.islogin);

            this.rootLogin = JSON.parse(localStorage.getItem('details')) || {};
        } else {
            this.islogin = JSON.parse(localStorage.getItem('details')) || {};
            console.log("islogin devices => " + JSON.stringify(this.islogin));
        }

    }
    dismiss() {
        this.viewCtrl.dismiss();
    }

    ngOnInit() {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
        this.getDrivers();
    }

    getDrivers() {
        this.apiCall.getDriverList(this.islogin._id)
            .subscribe(data => {
                this.driverList = data;
                console.log("driver list => " + JSON.stringify(this.driverList))
            },
                err => {
                    console.log(err);
                });
    }


    openAddGroupModal() {
        let modal = this.modalCtrl.create('GroupModalPage');
        modal.onDidDismiss(() => {
            console.log("modal dismissed!")
        })
        modal.present();
    }

    adddevices() {
        this.submitAttempt = true;
        if (this.addvehicleForm.valid) {
            console.log(this.addvehicleForm.value);
            if (this.params.get("custDet")) {
                if (this.rootLogin.isSuperAdmin == true) {
                    this.devicedetails = {
                        "devicename": this.addvehicleForm.value.device_name,
                        "deviceid": this.addvehicleForm.value.device_id,
                        "driver_name": this.addvehicleForm.value.driver,
                        "contact_number": this.addvehicleForm.value.contact_num,
                        "typdev": "Tracker",
                        "sim_number": this.addvehicleForm.value.sim_number,
                        "user": this.islogin._id,
                        "emailid": this.islogin.email,
                        "iconType": this.TypeOf_Device,
                        // "vehicleGroup": this.groupstaus_id,
                        "device_model": this.modeldata_id,
                        "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                        "supAdmin": this.rootLogin._id
                    }
                } else {
                    if (this.rootLogin.isDealer == true) {
                        this.devicedetails = {
                            "devicename": this.addvehicleForm.value.device_name,
                            "deviceid": this.addvehicleForm.value.device_id,
                            "driver_name": this.addvehicleForm.value.driver,
                            "contact_number": this.addvehicleForm.value.contact_num,
                            "typdev": "Tracker",
                            "sim_number": this.addvehicleForm.value.sim_number,
                            "user": this.islogin._id,
                            "emailid": this.islogin.email,
                            "iconType": this.TypeOf_Device,
                            // "vehicleGroup": this.groupstaus_id,
                            "device_model": this.modeldata_id,
                            "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                            "supAdmin": this.rootLogin.supAdmin,
                            "Dealer": this.rootLogin._id
                        }
                    }
                }

                if (this.addvehicleForm.value.fastag != null || this.addvehicleForm.value.fastag != undefined
                    || this.addvehicleForm.value.fastag != '') {
                    this.devicedetails.fastag = this.addvehicleForm.value.fastag
                }

                if (this.groupstaus != undefined) {
                    this.groupstaus_id = this.groupstaus._id;
                    this.devicedetails.vehicleGroup = this.groupstaus_id
                }

                if (this.vehType == undefined) {
                    this.devicedetails;
                } else {
                    this.devicedetails.vehicleType = this.vehType._id;
                }
            } else {
                if (this.islogin.isSuperAdmin == true) {
                    this.devicedetails = {
                        "devicename": this.addvehicleForm.value.device_name,
                        "deviceid": this.addvehicleForm.value.device_id,
                        "driver_name": this.addvehicleForm.value.driver,
                        "contact_number": this.addvehicleForm.value.contact_num,
                        "typdev": "Tracker",
                        "sim_number": this.addvehicleForm.value.sim_number,
                        "user": this.islogin._id,
                        "emailid": this.islogin.email,
                        "iconType": this.TypeOf_Device,
                        // "vehicleGroup": this.groupstaus_id,
                        "device_model": this.modeldata_id,
                        "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                        "supAdmin": this.islogin._id
                    }
                } else {
                    if (this.islogin.isDealer == true) {
                        this.devicedetails = {
                            "devicename": this.addvehicleForm.value.device_name,
                            "deviceid": this.addvehicleForm.value.device_id,
                            "driver_name": this.addvehicleForm.value.driver,
                            "contact_number": this.addvehicleForm.value.contact_num,
                            "typdev": "Tracker",
                            "sim_number": this.addvehicleForm.value.sim_number,
                            "user": this.islogin._id,
                            "emailid": this.islogin.email,
                            "iconType": this.TypeOf_Device,
                            // "vehicleGroup": this.groupstaus_id,
                            "device_model": this.modeldata_id,
                            "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                            "supAdmin": this.islogin.supAdmin,
                            "Dealer": this.islogin._id
                        }
                    }
                }

                if (this.addvehicleForm.value.fastag != null || this.addvehicleForm.value.fastag != undefined
                    || this.addvehicleForm.value.fastag != '') {
                    this.devicedetails.fastag = this.addvehicleForm.value.fastag
                }

                if (this.groupstaus != undefined) {
                    this.groupstaus_id = this.groupstaus._id;
                    this.devicedetails.vehicleGroup = this.groupstaus_id
                }

                if (this.vehType == undefined) {
                    this.devicedetails;
                } else {
                    this.devicedetails.vehicleType = this.vehType._id;
                }
            }


            // if (this.groupstaus == undefined) {
            //     this.groupstaus_id = this.islogin._id;

            // } else {
            //     this.groupstaus_id = this.groupstaus._id;
            // }



            console.log("devicedetails=> " + this.devicedetails);

            this.apiCall.startLoading().present();
            this.apiCall.addDeviceCall(this.devicedetails)
                .subscribe(data => {
                    this.apiCall.stopLoading();
                    this.devicesadd = data;
                    console.log("devicesadd=> ", this.devicesadd)
                    let toast = this.toastCtrl.create({
                        message: 'Vehicle was added successfully',
                        position: 'top',
                        duration: 1500
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        this.viewCtrl.dismiss(this.vehType);
                    });

                    toast.present();
                },
                    err => {
                        // this.apiCall.stopLoading();
                        // var body = err._body;
                        // var msg = JSON.parse(body);
                        // let alert = this.alerCtrl.create({
                        //     title: 'Oops!',
                        //     message: msg.errmsg,
                        //     buttons: ['Try Again']
                        // });
                        // alert.present();
                        // console.log("err");
                        this.apiCall.stopLoading();
                        // console.log("error occured=> ", err)
                        // console.log("error occured=> ", JSON.stringify(err))
                        var body = err._body;
                        var msg = JSON.parse(body);
                        console.log("error occured 1=> ", msg)
                        var a = msg.errors;
                        console.log("error occured 2=> ", a)
                        var b = a.type_of_device;
                        console.log("error occured 3=> ", b)

                        // var c = JSON.parse(b)
                        let alert = this.alerCtrl.create({
                            title: 'Oops!',
                            message: b.message,
                            buttons: ['Try Again']
                        });
                        alert.present();
                        console.log("err");
                    });
        }
    }

    getGroup() {
        console.log("get group");
        var baseURLp = this.apiCall.mainUrl + 'groups/getGroups_list?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp)
            .subscribe(data => {
                this.apiCall.stopLoading();
                // this.allGroup = data;
                this.allGroup = data["group_details"]
                // console.log("all group=> " + this.allGroup[0].name);
                // for (var i = 0; i < this.allGroup.length; i++) {
                //     this.allGroupName = this.allGroup[i].name;
                //     // console.log("allGroupName=> "+this.allGroupName);
                // }
                // console.log("allGroupName=> " + this.allGroupName)
            },
                err => {
                    console.log(err);
                    this.apiCall.stopLoading();
                });
    }

    getDeviceModel() {
        console.log("getdevices");
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDeviceModel';
        this.apiCall.getDeviceModelCall(baseURLp)
            .subscribe(data => {
                this.deviceModel = data;
            },
                err => {
                    console.log(err);
                });
    }

    getSelectUser() {
        console.log("get user");

        var baseURLp = this.apiCall.mainUrl + 'users/getAllUsers?dealer=' + this.islogin._id;

        this.apiCall.getAllUsersCall(baseURLp)
            .subscribe(data => {
                this.selectUser = data;
            },
                error => {
                    console.log(error)
                });
    }

    getVehicleType() {
        console.log("get getVehicleType");

        var baseURLp = this.apiCall.mainUrl + 'vehicleType/getVehicleTypes?user=' + this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getVehicleTypesCall(baseURLp)
            .subscribe(data => {
                // this.apiCall.stopLoading();
                this.allVehicle = data;
            }, err => {
                console.log(err);
                // this.apiCall.stopLoading();
            });

    }

    deviceModelata(deviceModel) {
        console.log("deviceModel" + deviceModel);

        if (deviceModel != undefined) {
            this.modeldata = deviceModel;
            this.modeldata_id = this.modeldata._id;
        } else {
            this.modeldata_id = null;
        }
        // console.log("modal data device_type=> " + this.modeldata.device_type);
    }

    GroupStatusdata(status) {
        console.log(status);
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    }

    userselectData(userselect) {
        console.log(userselect);
        this.userdata = userselect;
        console.log("userdata=> " + this.userdata.first_name);
    }

    vehicleTypeselectData(vehicletype) {
        console.log(vehicletype);
        this.vehType = vehicletype;
        console.log("vehType=> " + this.vehType._id);
    }
}