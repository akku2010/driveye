import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { SMS } from '@ionic-native/sms';
import * as moment from 'moment';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController, PopoverController, ViewController, Navbar, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-add-devices',
  templateUrl: 'add-devices.html',
})
export class AddDevicesPage implements OnInit {
  @ViewChildren("step") steps: QueryList<ElementRef>;
  islogin: any;
  isDealer: any;
  islogindealer: string;
  stausdevice: string;
  device_types: { vehicle: string; name: string; }[];
  GroupType: { vehicle: string; name: string; }[];
  GroupStatus: { name: string; }[];
  allDevices: any = [];
  allDevicesSearch: any = [];

  socket: any;
  veh: any;
  isdevice: string;
  userPermission: any;
  option_switch: boolean = false;
  dataEngine: any;
  DeviceConfigStatus: any;
  messages: string;
  editdata: any;
  responseMessage: string;
  searchCountryString = ''; // initialize your searchCountryString string empty
  countries: any;
  latLang: any;

  @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
  @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;
  @ViewChild(Navbar) navBar: Navbar;
  page: number = 0;
  limit: number = 5;
  ndata: any;
  searchItems: any;
  searchQuery: string = '';
  items: any[];
  deivceId: any;
  immobType: any;
  respMsg: any;
  commandStatus: any;
  intervalID: any;
  now: any;
  datePipe: any;
  isSuperAdmin: any;
  clicked: boolean = false;
  checkedPass: string;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private sms: SMS,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private geocoderApi: GeocoderProvider,
    private plt: Platform,
    public translate: TranslateService
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    console.log("fuel unit => " + this.islogin.fuel_unit);
    this.isDealer = this.islogin.isDealer;
    this.isSuperAdmin = this.islogin.isSuperAdmin;
    this.islogindealer = localStorage.getItem('isDealervalue');

    if (navParams.get("label") && navParams.get("value")) {
      this.stausdevice = localStorage.getItem('status');
    } else {
      this.stausdevice = undefined;
    }
  }

  fonctionTest(d) {
    var theft;
    theft = !(d.theftAlert);
    if (theft) {
      let alert = this.alertCtrl.create({
        title: this.translate.instant('Confirm'),
        message: this.translate.instant('Are you sure you want to activate anti theft alarm? On activating this alert you will get receive notification if vehicle moves.'),
        buttons: [
          {
            text: this.translate.instant('YES PROCEED'),
            handler: () => {
              var payload = {
                "_id": d._id,
                "theftAlert": theft
              }
              this.apiCall.startLoading();
              this.apiCall.deviceupdateCall(payload)
                .subscribe(data => {
                  this.apiCall.stopLoading();
                  let toast = this.toastCtrl.create({
                    message: this.translate.instant('Anti theft alarm Activated!'),
                    position: "bottom",
                    duration: 1000
                  });
                  toast.present();
                  this.getdevices();
                })
            }
          },
          {
            text: 'BACK',
            handler: () => {
              d.theftAlert = !(d.theftAlert);
            }
          }
        ]
      })
      alert.present();
    } else {
      let alert = this.alertCtrl.create({
        title: this.translate.instant('Confirm'),
        message: this.translate.instant('Are you sure you want to deactivate anti theft alarm?'),
        buttons: [
          {
            text: this.translate.instant('YES PROCEED'),
            handler: () => {
              // theft = d.theftAlert;
              var payload = {
                "_id": d._id,
                "theftAlert": theft
              }
              this.apiCall.startLoading();
              this.apiCall.deviceupdateCall(payload)
                .subscribe(data => {
                  this.apiCall.stopLoading();
                  console.log("resp: ", data)
                  let toast = this.toastCtrl.create({
                    message: this.translate.instant('Anti theft alarm Deactivated!'),
                    position: "bottom",
                    duration: 1000
                  });
                  toast.present();
                  this.getdevices();
                })
            }
          },
          {
            text: 'BACK',
            handler: () => {
              d.theftAlert = !(d.theftAlert);
            }
          }
        ]
      })
      alert.present();
    }
  }

  ngOnInit() {
    this.now = new Date().toISOString();
  }

  ionViewDidLoad() {
    this.getdevices();
    if (localStorage.getItem("SCREEN") != null) {
      this.navBar.backButtonClick = (e: UIEvent) => {
        if (localStorage.getItem("SCREEN") != null) {
          if (localStorage.getItem("SCREEN") === 'live') {
            this.navCtrl.setRoot('LivePage');
          } else {
            if (localStorage.getItem("SCREEN") === 'dashboard') {
              this.navCtrl.setRoot('DashboardPage')
            }
          }
        }
      }
    }
  }
  ionViewWillEnter() {
    this.getdevicesTemp();
  }

  activateVehicle(data) {
    this.navCtrl.push("PaytmwalletloginPage", {
      "param": data
    })
  }

  timeoutAlert() {
    let alerttemp = this.alertCtrl.create({
      message: "the server is taking much time to respond. Please try in some time.",
      buttons: [{
        text: this.translate.instant('Okay'),
        handler: () => {
          this.navCtrl.setRoot("DashboardPage");
        }
      }]
    });
    alerttemp.present();
  }

  showVehicleDetails(vdata) {
    this.navCtrl.push('VehicleDetailsPage', {
      param: vdata,
      option_switch: this.option_switch
    });
  }

  doRefresh(refresher) {
    let that = this;
    that.page = 0;
    that.limit = 5;
    console.log('Begin async operation', refresher);
    this.getdevices();
    refresher.complete();
  }

  shareVehicle(d_data) {
    let that = this;
    const prompt = this.alertCtrl.create({
      title: 'Share Vehicle',
      inputs: [
        {
          name: 'device_name',
          value: d_data.Device_Name
        },
        {
          name: 'shareId',
          placeholder: this.translate.instant('Enter Email Id/Mobile Number')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: () => {
          }
        },
        {
          text: this.translate.instant('Share'),
          handler: (data) => {
            that.sharedevices(data, d_data)

          }
        }
      ]
    });
    prompt.present();
  }

  sharedevices(data, d_data) {
    let that = this;
    var devicedetails = {
      "did": d_data._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  showDeleteBtn(b) {
    // debugger;
    let that = this;
    if (localStorage.getItem('isDealervalue') == 'true') {
      return false;
    } else {
      if (b) {
        var u = b.split(",");
        for (let p = 0; p < u.length; p++) {
          if (that.islogin._id == u[p]) {
            return true;
          }
        }
      }
      else {
        return false;
      }
    }

  }

  sharedVehicleDelete(device) {
    let that = this;
    that.deivceId = device;
    let alert = that.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this share vehicle ?'),
      buttons: [{
        text: this.translate.instant('YES PROCEED'),
        handler: () => {
          that.removeDevice(that.deivceId._id);
        }
      },
      {
        text: this.translate.instant('NO')
      }]
    });
    alert.present();
  }

  removeDevice(did) {
    this.apiCall.startLoading().present();
    this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
      .subscribe(data => {
        console.log(data)
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Shared Device was deleted successfully!'),
          duration: 1500
        });
        toast.onDidDismiss(() => {
          this.getdevices();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  showSharedBtn(a, b) {
    // debugger
    if (b) {
      return !(b.split(",").indexOf(a) + 1);
    }
    else {
      return true;
    }
  }

  presentPopover(ev, data) {
    console.log("populated=> " + JSON.stringify(data))
    let popover = this.popoverCtrl.create(PopoverPage,
      {
        vehData: data
      },
      {
        cssClass: 'contact-popover'
      });

    popover.onDidDismiss(() => {
      this.getdevices();
    })

    popover.present({
      ev: ev
    });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      var baseURLp;
      if (that.stausdevice) {
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
      }
      else {
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
      }

      if (this.islogin.isSuperAdmin == true) {
        baseURLp += '&supAdmin=' + this.islogin._id;
      } else {
        if (this.islogin.isDealer == true) {
          baseURLp += '&dealer=' + this.islogin._id;
        }
      }
      that.ndata = [];
      that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
        .subscribe(
          res => {
            that.ndata = res.devices;
            for (let i = 0; i < that.ndata.length; i++) {
              that.allDevices.push(that.ndata[i]);
            }
            that.allDevicesSearch = that.allDevices;
          },
          error => {
            console.log(error);
          });
      infiniteScroll.complete();
    }, 500);
  }

  getdevices() {
    var baseURLp;
    if (this.stausdevice) {
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
    }
    else {
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }

    // this.apiCall.toastMsgStarted();
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        // this.apiCall.toastMsgDismised();
        this.ndata = data.devices;
        this.allDevices = this.ndata;
        this.allDevicesSearch = this.ndata;
        let that = this;
        that.userPermission = JSON.parse(localStorage.getItem('details'));
        if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
          that.option_switch = true;
        } else {
          if (localStorage.getItem('isDealervalue') == 'true') {
            that.option_switch = true;
          } else {
            if (that.userPermission.isDealer == false) {
              that.option_switch = false;
            }
          }
        }
      },
        err => {
          console.log("error=> ", err);
          this.apiCall.stopLoading();
          // this.apiCall.toastMsgDismised();
        });
  }
  getdevicesTemp() {
    var baseURLp;
    if (this.stausdevice) {
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
    }
    else {
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }

    // this.apiCall.toastMsgStarted();
    // this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        // this.apiCall.stopLoading();
        // this.apiCall.toastMsgDismised();
        this.ndata = data.devices;
        this.allDevices = this.ndata;
        this.allDevicesSearch = this.ndata;
        let that = this;
        that.userPermission = JSON.parse(localStorage.getItem('details'));
        if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
          that.option_switch = true;
        } else {
          if (localStorage.getItem('isDealervalue') == 'true') {
            that.option_switch = true;
          } else {
            if (that.userPermission.isDealer == false) {
              that.option_switch = false;
            }
          }
        }
      },
        err => {
          console.log("error=> ", err);
          // this.apiCall.stopLoading();
          // this.apiCall.toastMsgDismised();
        });
  }

  livetrack(device) {
    localStorage.setItem("LiveDevice", "LiveDevice");
    let animationsOptions;
    if (this.plt.is('android')) {
      this.navCtrl.push('LiveSingleDevice', { device: device });
    } else {
      if (this.plt.is('ios')) {
        animationsOptions = {
          animation: 'ios-transition',
          duration: 1000
        }
        this.navCtrl.push('LiveSingleDevice', { device: device }, animationsOptions);
      }
    }
  }

  showHistoryDetail(device) {
    this.navCtrl.push('HistoryDevicePage', {
      device: device
    })
  }

  device_address(device, index) {
    let that = this;
    that.allDevices[index].address = "N/A";
    if (!device.last_location) {
      that.allDevices[index].address = "N/A";
    } else if (device.last_location) {
      this.geocoderApi.reverseGeocode(Number(device.last_location.lat), Number(device.last_location.long))
        .then((res) => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
          that.allDevices[index].address = str;
        })
    }
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apiCall.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  getDuration(device, index) {
    let that = this;
    that.allDevices[index].duration = "0hrs 0mins";
    if (!device.status_updated_at) {
      that.allDevices[index].duration = "0hrs 0mins";
    } else if (device.status_updated_at) {
      var a = moment(new Date().toISOString());//now
      var b = moment(new Date(device.status_updated_at).toISOString());
      var mins;
      mins = a.diff(b, 'minutes') % 60;
      that.allDevices[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
      console.log('Current time: ', new Date().toISOString());
      console.log('last updated time: ', new Date(device.status_updated_at).toISOString());
      console.log('duration: ', that.allDevices[index].duration)
      // console.log(a.diff(b, 'minutes')) // 44700
      // console.log(a.diff(b, 'hours')) // 745
      // console.log(a.diff(b, 'days')) // 31
      // console.log(a.diff(b, 'weeks')) // 4
    }
  }

  callSearch(ev) {
    var searchKey = ev.target.value;
    var _baseURL;

    if (this.islogin.isDealer == true) {
      _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
    } else {
      if (this.islogin.isSuperAdmin == true) {
        _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
      } else {
        _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
      }
    }

    this.apiCall.callSearchService(_baseURL)
      .subscribe(data => {
        // this.apiCall.stopLoading();
        console.log("search result=> " + JSON.stringify(data))
        this.allDevicesSearch = data.devices;
        this.allDevices = data.devices;
        console.log("fuel percentage: " + data.devices[0].fuel_percent)
      },
        err => {
          console.log(err);
          // this.apiCall.stopLoading();
        });
  }

  checkImmobilizePassword() {
    const rurl = this.apiCall.mainUrl + 'users/get_user_setting';
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(rurl, Var)
      .subscribe(data => {
        if (!data.engine_cut_psd) {
          this.checkedPass = 'PASSWORD_NOT_SET';
        } else {
          this.checkedPass = 'PASSWORD_SET';
        }
      })
  }

  IgnitionOnOff(d) {
    let that = this;
    if (d.last_ACC != null || d.last_ACC != undefined) {

      if (that.clicked) {
        let alert = this.alertCtrl.create({
          message: this.translate.instant('Process ongoing..'),
          buttons: [this.translate.instant('Okay')]
        });
        alert.present();
      } else {
        this.checkImmobilizePassword();
        this.messages = undefined;
        this.dataEngine = d;
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
          .subscribe(data => {
            this.apiCall.stopLoading();
            this.DeviceConfigStatus = data;
            this.immobType = data[0].imobliser_type;
            if (this.dataEngine.ignitionLock == '1') {
              this.messages = this.translate.instant('Do you want to unlock the engine?')
            } else {
              if (this.dataEngine.ignitionLock == '0') {
                this.messages = this.translate.instant('Do you want to lock the engine?')
              }
            }
            let alert = this.alertCtrl.create({
              message: this.messages,
              buttons: [{
                text: 'YES',
                handler: () => {
                  if (this.immobType == 0 || this.immobType == undefined) {
                    that.clicked = true;
                    var devicedetail = {
                      "_id": this.dataEngine._id,
                      "engine_status": !this.dataEngine.engine_status
                    }
                    // this.apiCall.startLoading().present();
                    this.apiCall.deviceupdateCall(devicedetail)
                      .subscribe(response => {
                        // this.apiCall.stopLoading();
                        this.editdata = response;
                        const toast = this.toastCtrl.create({
                          message: response.message,
                          duration: 2000,
                          position: 'top'
                        });
                        toast.present();
                        // this.responseMessage = "Edit successfully";
                        this.getdevices();

                        var msg;
                        if (!this.dataEngine.engine_status) {
                          msg = this.DeviceConfigStatus[0].resume_command;
                        }
                        else {
                          msg = this.DeviceConfigStatus[0].immoblizer_command;
                        }

                        this.sms.send(d.sim_number, msg);
                        const toast1 = this.toastCtrl.create({
                          message: this.translate.instant('SMS sent successfully'),
                          duration: 2000,
                          position: 'bottom'
                        });
                        toast1.present();
                        that.clicked = false;
                      },
                        error => {
                          that.clicked = false;
                          // this.apiCall.stopLoading();
                          console.log(error);
                        });
                  } else {
                    console.log("Call server code here!!")
                    if (that.checkedPass === 'PASSWORD_SET') {
                      this.askForPassword(d);
                      return;
                    }
                    that.serverLevelOnOff(d);
                  }
                }
              },
              {
                text: this.translate.instant('NO')
              }]
            });
            alert.present();
          },
            error => {
              this.apiCall.stopLoading();
              console.log("some error: ", error._body.message);
            });
      }
    }
  };

  askForPassword(d) {
    const prompt = this.alertCtrl.create({
      title: 'Enter Password',
      message: "Enter password for engine cut",
      inputs: [
        {
          name: 'password',
          placeholder: 'Password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Proceed',
          handler: data => {
            console.log('Saved clicked');
            console.log("data: ", data)
            // if (data.password !== data.cpassword) {
            //   this.toastmsg("Entered password and confirm password did not match.")
            //   return;
            // }
            this.verifyPassword(data, d);
          }
        }
      ]
    });
    prompt.present();
  }

  verifyPassword(pass, d) {
    const ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
    var payLd = {
      "uid": this.islogin._id,
      "psd": pass.password
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(ryurl, payLd)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        console.log(resp);
        if (resp.message === 'password not matched') {
          this.toastmsg(resp.message)
          return;
        }
        this.serverLevelOnOff(d);
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  toastmsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    }).present();
  }

  serverLevelOnOff(d) {
    let that = this;
    that.clicked = true;
    var data = {
      "imei": d.Device_ID,
      "_id": this.dataEngine._id,
      "engine_status": d.ignitionLock,
      "protocol_type": d.device_model.device_type
    }
    // this.apiCall.startLoading().present();
    this.apiCall.serverLevelonoff(data)
      .subscribe(resp => {
        // this.apiCall.stopLoading();
        console.log("ignition on off=> ", resp)
        this.respMsg = resp;

        // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
        this.intervalID = setInterval(() => {
          this.apiCall.callResponse(this.respMsg._id)
            .subscribe(data => {
              console.log("interval=> " + data)
              this.commandStatus = data.status;

              if (this.commandStatus == 'SUCCESS') {
                clearInterval(this.intervalID);
                that.clicked = false;
                // this.apiCall.stopLoadingnw();
                const toast1 = this.toastCtrl.create({
                  message: this.translate.instant('process has been completed successfully!'),
                  duration: 1500,
                  position: 'bottom'
                });
                toast1.present();
                this.getdevices();
              }
            })
        }, 5000);
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error in onoff=>", err);
          that.clicked = false;
        });
  }

  dialNumber(number) {
    window.open('tel:' + number, '_system');
  }

  getItems(ev: any) {
    const val = ev.target.value.trim();
    this.allDevicesSearch = this.allDevices.filter((item) => {
      return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    console.log("search====", this.allDevicesSearch);
  }

  onClear(ev) {
    this.getdevices();
    ev.target.value = '';
  }

  openAdddeviceModal() {
    let profileModal = this.modalCtrl.create('AddDeviceModalPage');
    profileModal.onDidDismiss(data => {
      console.log(data);
      this.getdevices();
    });
    profileModal.present();
  }

  upload(vehData) {
    this.navCtrl.push('UploadDocPage', { vehData: vehData });
  }
}

@Component({
  template: `
    <ion-list>
      <ion-item class="text-palatino" (click)="editItem()">
        <ion-icon name="create"></ion-icon>&nbsp;&nbsp;{{'edit' | translate}}
      </ion-item>
      <ion-item class="text-san-francisco" (click)="deleteItem()">
        <ion-icon name="trash"></ion-icon>&nbsp;&nbsp;{{'delete' | translate}}
      </ion-item>
      <ion-item class="text-seravek" (click)="shareItem()">
        <ion-icon name="share"></ion-icon>&nbsp;&nbsp;{{'share' | translate}}
      </ion-item>
    </ion-list>
  `
})


export class PopoverPage implements OnInit {
  contentEle: any;
  textEle: any;
  vehData: any;
  islogin: any;
  veh: any;
  constructor(
    navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public translate: TranslateService
  ) {
    this.vehData = navParams.get("vehData");
    console.log("popover data=> ", this.vehData);

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + this.islogin);
  }

  ngOnInit() { }

  editItem() {
    console.log("edit")
    let modal = this.modalCtrl.create('UpdateDevicePage', {
      vehData: this.vehData
    });
    modal.onDidDismiss(() => {
      console.log("modal dismissed!")
      this.viewCtrl.dismiss();
    })
    modal.present();
  }

  // valScreen(d_id) {
  //   let modal = this.modalCtrl.create('DailyReportNewPage', {
  //     vehData: this.vehData
  //   });
  //   console.log(this.vehData);
  //   modal.onDidDismiss(() => {
  //     console.log("modal dismissed!")
  //     this.viewCtrl.dismiss();
  //   })
  //   modal.present();
  // }

  deleteItem() {
    let that = this;
    console.log("delete")
    let alert = this.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this vehicle ?'),
      buttons: [{
        text: this.translate.instant('YES PROCEED'),
        handler: () => {
          console.log(that.vehData.Device_ID)
          that.deleteDevice(that.vehData.Device_ID);
        }
      },
      {
        text: this.translate.instant('NO')
      }]
    });
    alert.present();
  }


  deleteDevice(d_id) {
    this.apiCall.startLoading().present();
    this.apiCall.deleteDeviceCall(d_id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        var DeletedDevice = data;
        console.log(DeletedDevice);

        let toast = this.toastCtrl.create({
          message: this.translate.instant('Vehicle deleted successfully!'),
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.viewCtrl.dismiss();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  shareItem() {
    let that = this;
    console.log("share")
    const prompt = this.alertCtrl.create({
      title: this.translate.instant('Share Vehicle'),
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'device_name',
          value: that.vehData.Device_Name
        },
        {
          name: 'shareId',
          placeholder: this.translate.instant('Enter Email Id/Mobile Number')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('Share'),
          handler: data => {
            console.log('Saved clicked');
            console.log("clicked=> ", data)

            that.sharedevices(data)

          }
        }
      ]
    });
    prompt.present();
  }

  // valScreen() {
  //   let that = this
  //   console.log("selected=>", that.vehData);
  //   localStorage.selectedVal = JSON.stringify(that.vehData);
  //   this.navCtrl.push('DailyReportNewPage');
  //   // if(this.vehData == "Device_Name" ) {
  //   //   console.log(this.vehData);
  //   //   this.navCtrl.push('DailyReportNewPage');
  //   // }
  // }


  sharedevices(data) {
    let that = this;
    console.log(data.shareId);
    var devicedetails = {
      "did": that.vehData._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        // var editdata = data;
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

}
