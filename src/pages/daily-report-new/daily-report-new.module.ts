import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyReportNewPage } from './daily-report-new';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    DailyReportNewPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyReportNewPage),
    SelectSearchableModule,
  ],
})
export class DailyReportNewPageModule {}
