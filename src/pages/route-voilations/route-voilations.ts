import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-route-voilations',
  templateUrl: 'route-voilations.html',
})
export class RouteVoilationsPage implements OnInit {

  islogin: any;
  datetimeStart: string;
  datetimeEnd: string;
  devices1243: any[];
  routelist: any;
  routevolitionReport: any;
  routename_id: any;
  datetime: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apicallroute: ApiServiceProvider,
    public alertCtrl: AlertController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
   
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RouteVoilationsPage');
  }


  ngOnInit() {
    this.getRoute();
  }

  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }

  getRoute() {

    var baseURLp = 'https://www.oneqlik.in/trackRoute/user/' + this.islogin._id;
    this.apicallroute.startLoading().present();
    this.apicallroute.getallrouteCall(baseURLp)
      .subscribe(data => {
        this.apicallroute.stopLoading();
        this.devices1243 = [];
        this.routelist = data;
        console.log("Routelist=> ", this.routelist)

      },
        err => {
          this.apicallroute.stopLoading();
          console.log(err)
        });
  }

  getRouteName(from, to, selectedroute) {
    console.log("selectedVehicle=> ", selectedroute)
    this.routename_id = selectedroute.Device_Name;
  }


  getroutevoilation(starttime, endtime) {
    var baseURLp = 'https://www.oneqlik.in/notifs/RouteVoilationReprot?from_date=' + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id;

    this.apicallroute.startLoading().present();

    this.apicallroute.getallrouteCall(baseURLp)
      .subscribe(data => {
        this.apicallroute.stopLoading();
        this.routevolitionReport = data;
        console.log(this.routevolitionReport);
        if (this.routevolitionReport.length == 0) {
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }

      }, error => {
        this.apicallroute.stopLoading();
        console.log(error);
      })

  }
}
