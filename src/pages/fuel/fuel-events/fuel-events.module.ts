import { NgModule } from "@angular/core";
import { OnCreate2Module } from "../fuel-consumption-report/dummy2-directive.module";
import { FuelEventsComponent } from "./fuel-events";
import { IonicPageModule } from "ionic-angular";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [
    FuelEventsComponent,
  ],
  imports: [
    IonicPageModule.forChild(FuelEventsComponent),
    OnCreate2Module,
    TranslateModule.forChild()
  ]
})
export class FuelEventsComponentModule {

}
