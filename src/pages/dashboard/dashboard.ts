import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform, ToastController, PopoverController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import '../../assets/js/chartjs-plugin-labels.min.js';
import * as io from 'socket.io-client';   // with ES6 import
import { PushObject, Push, PushOptions } from '@ionic-native/push';
import { LanguagesPage } from '../login/login';


@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  public cartCount: number = 0;

  islogin: any;
  uuid: string;
  PushToken: string;
  dealerStatus: any;
  @ViewChild('text1') text1;
  @ViewChild('chart') chart: any;
  text1Chart: any;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;
  @ViewChild('barCanvas') barCanvas;
  barChart: any;
  @ViewChild('doughnutCanvas1') doughnutCanvas1;
  doughnutChart1: any;
  from: any;
  to: any;
  TotalDevice: any[];
  devices: any;
  Running: any;
  IDLING: any;
  Stopped: any;
  Maintance: any;
  OutOffReach: any;
  NogpsDevice: any;
  totalVechiles: any;
  devices1243: any[];
  isdevice: string;
  socket: any;
  token: string;
  totaldevices: any;
  expiredDevices: any;
  realRemindersData: any;


  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public platform: Platform,
    private toastCtrl: ToastController,
    public push: Push,
    private popoverCtrl: PopoverController
  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    ///////////////////////////////
    this.events.publish('user:updated', this.islogin);
    /////////////////////////////////

    this.uuid = localStorage.getItem('uuid');
    this.PushToken = localStorage.getItem('PushToken');
    this.dealerStatus = this.islogin.isDealer;

    this.to = new Date().toISOString();
    var d = new Date();
    var a = d.setHours(0, 0, 0, 0)
    this.from = new Date(a).toISOString();

    ///////////////=======Push notifications========////////////////
    this.events.subscribe('cart:updated', (count) => {
      this.cartCount = count;
    });

    this.socket = io(this.apiCall.mainUrl + 'notifIO', {
      transports: ['websocket', 'polling']
    });

    this.socket.on('connect', () => {
      console.log('IO Connected tabs');
      console.log("socket connected tabs", this.socket.connected)
    });

    this.socket.on(this.islogin._id, (msg) => {
      this.cartCount += 1;
    })

  }

  ionViewWillEnter() {
    this.plugin();
    this.getDashboarddevices();
    // this.navCtrl.setRoot('DashboardPage');
  }

  ionViewDidLoad() {
    // this.navCtrl.setRoot('DashboardPage');
    this.presentPopover();
    // this.secondSlide();
    // this.thirdSlide();
    localStorage.removeItem("LiveDevice");
  }

  doRefresh(refresher) {
    this.getDashboarddevices();
    refresher.complete();
  }

  seeNotifications() {
    this.navCtrl.push('AllNotificationsPage');
  }

  secondSlide() {
    var role;
    if (this.islogin.isSuperAdmin === true) {
      role = 'supAdmin';
    } else if (this.islogin.isDealer === true) {
      role = 'Dealer';
    } else {
      role = 'User';
    }
    var _burl = this.apiCall.mainUrl + "gps/vehicleUtilizationPercentage?role=" + role + "&id=" + this.islogin._id;
    this.apiCall.getSOSReportAPI(_burl)
      .subscribe(resp => {
        console.log('vehicle utilization data => ', resp);
        if (resp) {
          this.secondChart(resp);
          this.thirdSlide();
        }
      },
        err => {
          console.log(err);
        });
  }

  secondChart(bargraph) {
    if (this.barChart) {
      this.barChart.destroy();
    }

    if (bargraph.length != 0) {
      var varGraphObject = bargraph;

      for (let bg in varGraphObject) {
        var runningArr = [
          (varGraphObject[0]['Daily']['Running'] != undefined) ? (varGraphObject[0]['Daily']['Running']).toFixed(2) : 0,
          (varGraphObject[1]['Weekly']['Running'] != undefined) ? (varGraphObject[1]['Weekly']['Running']).toFixed(2) : 0,
          (varGraphObject[2]['Monthly']['Running'] != undefined) ? (varGraphObject[2]['Monthly']['Running']).toFixed(2) : 0
        ];

        var IdleArr = [
          (varGraphObject[0]['Daily']['Idle'] != undefined) ? (varGraphObject[0]['Daily']['Idle']).toFixed(2) : 0,
          (varGraphObject[1]['Weekly']['Idle'] != undefined) ? (varGraphObject[1]['Weekly']['Idle']).toFixed(2) : 0,
          (varGraphObject[2]['Monthly']['Idle'] != undefined) ? (varGraphObject[2]['Monthly']['Idle']).toFixed(2) : 0
        ];
        var OFRArr = [
          (varGraphObject[0]['Daily']['OFR'] != undefined) ? (varGraphObject[0]['Daily']['OFR']).toFixed(2) : 0,
          (varGraphObject[1]['Weekly']['OFR'] != undefined) ? (varGraphObject[1]['Weekly']['OFR']).toFixed(2) : 0,
          (varGraphObject[2]['Monthly']['OFR'] != undefined) ? (varGraphObject[2]['Monthly']['OFR']).toFixed(2) : 0
        ];
        var StopArr = [
          (varGraphObject[0]['Daily']['Stop'] != undefined) ? (varGraphObject[0]['Daily']['Stop']).toFixed(2) : 0,
          (varGraphObject[1]['Weekly']['Stop'] != undefined) ? (varGraphObject[1]['Weekly']['Stop']).toFixed(2) : 0,
          (varGraphObject[2]['Monthly']['Stop'] != undefined) ? (varGraphObject[2]['Monthly']['Stop']).toFixed(2) : 0
        ];
      }

      var barChartData = {
        labels: [
          "Today",
          "Week",
          "Month"
        ],
        datasets: [
          {
            label: "Running",
            backgroundColor: "#11a46e",
            borderColor: "white",
            borderWidth: 1,
            data: [runningArr[0], runningArr[1], runningArr[2]]
          },
          {
            label: "Out of Reach",
            backgroundColor: "#1bb6d4",
            borderColor: "white",
            borderWidth: 1,
            data: [OFRArr[0], OFRArr[1], OFRArr[2]]
          },
          {
            label: "Idle",
            backgroundColor: "#ffc13c",
            borderColor: "white",
            borderWidth: 1,
            data: [IdleArr[0], IdleArr[1], IdleArr[2]]
          },
          {
            label: "Stopped",
            backgroundColor: "#ff3f32",
            borderColor: "white",
            borderWidth: 1,
            data: [StopArr[0], StopArr[1], StopArr[2]]
          }
        ]
      };

      var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        title: {
          display: true,
          fontSize: 14,
          text: 'Vehicle Utilization(%)'
        },
        plugins: {
          labels: {
            render: 'value',
            fontSize: 0,
            fontColor: '#fff',
            images: [
              {
                src: 'image.png',
                width: 16,
                height: 16
              }
            ],
            outsidePadding: 2,
            textMargin: 4
          }
        },

        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              max: 100,
              min: 0
            }
          }]
        }
      }
    }
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "bar",
      data: barChartData,
      options: chartOptions
    });
  }

  thirdSlide() {
    var urlB = this.apiCall.mainUrl + 'expense/dashboardExpense?id=' + this.islogin._id;
    this.apiCall.getSOSReportAPI(urlB)
      .subscribe(resp => {
        console.log("expense chart: ", resp);
        this.thirdChart(resp);
        this.fourthSlide();
      },
        err => {
          console.log('error', err);
        });
  }

  thirdChart(data) {
    if (this.doughnutChart1) {
      this.doughnutChart1.destroy();
    }
    var expenseData = data;
    var expData = [0, 0, 0, 0, 0];
    var expLevel = ['Service',
      'Oil Changed',
      'Maintenance',
      'Tyres',
      'Others']
    if (expenseData.length != 0) {
      expData = [];
      expLevel = [];
      for (var i = 0; i < expenseData.length; i++) {
        expData.push(expenseData[i].amount);
        expLevel.push(expenseData[i]._id)
      }
    }
    console.log('expDataexpDataexpDataexpDataexpData', expData);
    var daugnoutChart_1 = {
      datasets: [{
        data: expData,
        backgroundColor: ['#86AC41', '#CB0000', '#F5BE41', '#4897D8', '#5B7065'],
      }],
      labels: expLevel
    };
    this.doughnutChart1 = new Chart(this.doughnutCanvas1.nativeElement, {
      type: 'pie',
      data: daugnoutChart_1,
      options: {
        title: {
          display: true,
          fontSize: 14,
          text: 'Vehicle Expense'
        },
        plugins: {
          labels: {
            render: 'value',
            fontSize: 0,
            fontColor: '#fff',
            images: [
              {
                src: 'image.png',
                width: 16,
                height: 16
              }
            ],
            outsidePadding: 2,
            textMargin: 4
          }
        },
        tooltips: {
          enabled: true
        },
        pieceLabel: {
          mode: 'value',
          fontColor: ['white', 'white', 'white', 'white', 'white']
        },
        responsive: true,
        legend: {
          display: true,
          labels: {
            fontColor: 'rgb(255, 99, 132)',
            fontSize: 10,
            fontStyle: 'bold',
            boxWidth: 10,
            usePointStyle: true
          },
          position: 'bottom',
        }
      }
    });
  }
  reminers: string = 'lastSeven';
  remindersData: any = [];
  fourthSlide() {
    this.reminers = 'lastSeven';
    this.remindersData = [];
    let url = this.apiCall.mainUrl + "reminder/dashboardReminder?id=" + this.islogin._id;
    this.apiCall.getSOSReportAPI(url)
      .subscribe(respData => {
        console.log("get reminders=> ", respData)
        this.realRemindersData = respData;
        var lastSeven = respData[0].seven;
        var lastFifteen = respData[1].fifteen;
        var lastThirty = respData[2].thirty;
        console.log("last seven length: ", lastSeven.length)
        console.log("last fifteen length: ", lastFifteen.length)
        console.log("last thirty length: ", lastThirty.length)
        if (lastSeven.length > 0) {
          this.remindersData = lastSeven;
        }
      },
        err => {
          console.log("error: ", err)
        })
  }

  // vehicleReminder() {
  //   var Uid = this.useridd;
  //   this.reminderrr = [];
  //   this.contactService.getVehicleReminder(Uid).subscribe(res => {
  //     console.log('resresresresresresres=>', res);
  //     this.reminderrr = res[0];
  //     var notifString = '';
  //     this.notifArr = [];
  //     if (this.reminderVal == '7days') {
  //       this.reminderrr = res[0]['seven'];
  //       if (this.reminderrr.length != 0) {
  //         let devName = '';
  //         let reminderType = '';
  //         let reminderDate = '';
  //         for (var i = 0; i < this.reminderrr.length; i++) {
  //           var rd = moment(this.reminderrr[i].reminder_date).format('lll');;
  //           notifString = 'Reminder for Vehicle' + ' ' + this.reminderrr[i].device.Device_Name + ' ' + 'for' + ' ' + this.reminderrr[i].reminder_type + ' ' + 'on' + ' ' + rd;
  //           this.notifArr.push(notifString);
  //           console.log('notifArr', this.notifArr);
  //         }
  //       }
  //       console.log('reminder for last 7 days', this.reminderrr);
  //     }
  //     if (this.reminderVal == '15days') {
  //       this.reminderrr = res[1]['fifteen'];
  //       if (this.reminderrr.length != 0) {
  //         let devName = '';
  //         let reminderType = '';
  //         let reminderDate = '';
  //         for (var i = 0; i < this.reminderrr.length; i++) {
  //           var rd = moment(this.reminderrr[i].reminder_date).format('lll');;
  //           notifString = 'Reminder for Vehicle' + ' ' + this.reminderrr[i].device.Device_Name + ' ' + 'for' + ' ' + this.reminderrr[i].reminder_type + ' ' + 'on' + ' ' + rd;
  //           this.notifArr.push(notifString);
  //           console.log('notifArr', this.notifArr);
  //         }
  //       }
  //     }

  //     if (this.reminderVal == '30days') {
  //       this.reminderrr = res[2]['thirty'];
  //       if (this.reminderrr.length != 0) {
  //         let devName = '';
  //         let reminderType = '';
  //         let reminderDate = '';
  //         for (var i = 0; i < this.reminderrr.length; i++) {
  //           var rd = moment(this.reminderrr[i].reminder_date).format('lll');;
  //           notifString = 'Reminder for Vehicle' + ' ' + this.reminderrr[i].device.Device_Name + ' ' + 'for' + ' ' + this.reminderrr[i].reminder_type + ' ' + 'on' + ' ' + rd;
  //           this.notifArr.push(notifString);
  //           console.log('notifArr', this.notifArr);
  //         }
  //       }
  //     }

  //   }, err => {
  //     console.log(err);
  //   })
  // }

  onReminersChange(val) {
    console.log(val)
    this.remindersData = [];
    var lastSeven = this.realRemindersData[0].seven;
    var lastFifteen = this.realRemindersData[1].fifteen;
    var lastThirty = this.realRemindersData[2].thirty;
    if (val === 'lastSeven') {
      this.remindersData = lastSeven;
    } else if (val === 'lastFifteen') {
      this.remindersData = lastFifteen;
    } else if (val === 'lastThirty') {
      this.remindersData = lastThirty;
    }
  }

  presentPopover() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (!resp.language_code) {
          let popover = this.popoverCtrl.create(LanguagesPage, {}, { enableBackdropDismiss: false });
          popover.present({});
        } else {
          this.events.publish('lang:key', resp.language_code);
        }
      },
        err => {
          console.log(err);
        });
  }

  plugin() {
    // let that = this;
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          var ctx = chart.chart.ctx;

          //Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = centerConfig.color || '#000';
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          // var width = chart.chart.width,
          //   height = chart.chart.height,
          //   ctx = chart.chart.ctx;

          // var d = Math.min(width, height);
          // var a = d / 2;

          // that.text1Chart.style.left = (((width - a) / 2 - 1) | 0) + "px";
          // that.text1Chart.style.top = (((height - a) / 2 - 1) | 0) + "px";
          // that.text1Chart.style.width = a + "px";
          // that.text1Chart.style.height = a + "px";
          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });
  }

  getChart() {
    let that = this;
    that.plugin();
    // if the chart is not undefined (e.g. it has been created)
    // then destory the old one so we can create a new one later
    if (this.doughnutChart) {
      this.doughnutChart.destroy();
    }

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

      type: 'doughnut',
      data: {
        labels: ["RUNNING", "IDLING", "STOPPED", "OUT OF REACH", "NO GPS FIX", "EXPIRED"],
        datasets: [{
          label: '# of Votes',
          data: that.TotalDevice,
          backgroundColor: [
            '#11a46e', '#ffc13c', '#ff3f32', '#1bb6d4', '#dadada', '#454344'
          ],
          hoverBackgroundColor: [
            'rgba(51, 255, 153, 0.7)',
            'rgba(255, 255, 51, 0.7)',
            'rgba(255, 99, 132, 0.7)',
            'rgba(204, 204, 179, 0.7)',
            'rgba(75, 192, 192, 0.7)',
            'rgba(255, 102, 51, 0.7)',
            'rgba(0,0,0,0.5)'
          ]
        }]
      },
      options: {
        title: {
          display: true,
          fontSize: 14,
          text: 'Vehicle Current Status'
        },
        plugins: {
          labels: {
            render: 'value',
            fontSize: 16,
            fontColor: '#fff',
            images: [
              {
                src: 'image.png',
                width: 16,
                height: 16
              }
            ],
            outsidePadding: 2,
            textMargin: 4
          }
        },
        elements: {
          center: {
            text: that.totaldevices,
            color: '#d80622', // Default is #000000
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 10, // Defualt is 20 (as a percentage),
          },

        },
        tooltips: {
          enabled: false
        },
        pieceLabel: {
          mode: 'value',
          fontColor: ['white', 'white', 'white', 'white', 'white']
        },
        responsive: true,
        legend: {
          display: true,
          labels: {
            fontColor: 'rgb(255, 99, 132)',
            fontSize: 10,
            fontStyle: 'bold',
            boxWidth: 10,
            usePointStyle: true
          },
          position: 'bottom',
        },
        onClick: function (event, elementsAtEvent) {
          var activePoints = elementsAtEvent;
          if (activePoints[0]) {
            var chartData = activePoints[0]['_chart'].config.data;
            var idx = activePoints[0]['_index'];
            var label = chartData.labels[idx];
            var value = chartData.datasets[0].data[idx];
            if (label === 'EXPIRED') {
              localStorage.setItem('status', 'Expired');
            } else if (label === 'NO GPS') {
              localStorage.setItem('status', 'NO GPS FIX');
            } else {
              localStorage.setItem('status', label);
            }
            that.navCtrl.push('AddDevicesPage', {
              label: label,
              value: value
            });
          }
        }
      }
    });
  }

  historyDevice() {
    localStorage.setItem("MainHistory", "MainHistory");
    this.navCtrl.push('HistoryDevicePage');
  }

  opennotify() {
    this.navCtrl.push('AllNotificationsPage')
  }

  addPushNotify() {
    console.log("VeryFirstLoginUser: ", localStorage.getItem("VeryFirstLoginUser"));
    let that = this;
    that.token = "";
    var pushdata;
    that.token = localStorage.getItem("DEVICE_TOKEN");
    var v_id = localStorage.getItem("VeryFirstLoginUser");
    if (that.token == null) {
      this.pushSetup();
    } else {
      if (this.platform.is('android')) {
        pushdata = {
          "uid": that.islogin._id,
          "token": that.token,
          "os": "android"
        }
      } else {
        pushdata = {
          "uid": that.islogin._id,
          "token": that.token,
          "os": "ios"
        }
      }

      if (v_id == that.islogin._id) {
        that.apiCall.pushnotifyCall(pushdata)
          .subscribe(data => {
            console.log("push notifications updated " + data.message)
          },
            error => {
              console.log(error);
            });
      }
      else {
        console.log("Token already updated!!")
      }
    }
  }

  pushSetup() {
    this.push.createChannel({
      id: "ignitionon",
      description: "ignition on description",
      sound: 'ignitionon',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('Channel created'));
    this.push.createChannel({
      id: "ignitionoff",
      description: "ignition off description",
      sound: 'ignitionoff',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('ignitionoff Channel created'));

    this.push.createChannel({
      id: "devicepowerdisconnected",
      description: "devicepowerdisconnected description",
      sound: 'devicepowerdisconnected',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('devicepowerdisconnected Channel created'));

    this.push.createChannel({
      id: "default",
      description: "default description",
      sound: 'default',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('default Channel created'));

    this.push.createChannel({
      id: "notif",
      description: "default description",
      sound: 'notif',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('sos Channel created'));
    // Delete a channel (Android O and above)
    // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));

    // Return a list of currently configured channels
    this.push.listChannels().then((channels) => console.log('List of channels', channels))
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      if (notification.additionalData.foreground) {
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        });
        toast.present();
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("dashboard entered device token => " + registration.registrationId);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        that.addPushNotify();
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
    });
  }

  getDashboarddevices() {
    console.log("getdevices");

    var _baseUrl = this.apiCall.mainUrl + 'gps/getDashboard?email=' + this.islogin.email + '&from=' + this.from + '&to=' + this.to + '&id=' + this.islogin._id;

    if (this.islogin.isSuperAdmin == true) {
      _baseUrl += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        _baseUrl += '&dealer=' + this.islogin._id;
      }
    }

    this.apiCall.dashboardcall(_baseUrl)
      .subscribe(data => {
        this.totaldevices = data.Total_Vech;
        ///// add push token to server //////////////
        this.addPushNotify();
        /////////////////////////////////////////////

        this.TotalDevice = [];
        this.devices = data;
        this.Running = this.devices.running_devices;
        this.TotalDevice.push(this.Running)
        this.IDLING = this.devices["Ideal Devices"]
        this.TotalDevice.push(this.IDLING);
        this.Stopped = this.devices["OFF Devices"]
        this.TotalDevice.push(this.Stopped);
        // this.Maintance = this.devices["Maintance Device"]
        // this.TotalDevice.push(this.Maintance);
        this.OutOffReach = this.devices["OutOfReach"]
        this.TotalDevice.push(this.OutOffReach);
        this.NogpsDevice = this.devices["no_gps_fix_devices"]
        this.TotalDevice.push(this.NogpsDevice);
        this.expiredDevices = this.devices["expire_status"];
        this.TotalDevice.push(this.expiredDevices);
        this.totalVechiles = this.Running + this.IDLING + this.Stopped + this.OutOffReach + this.NogpsDevice;
        this.getChart();
        this.secondSlide();


      },
        err => {
          console.log(err);
        });
  }

  vehiclelist() {
    console.log("coming soon");
    this.navCtrl.push('AddDevicesPage');
  }

  live() {
    if (this.platform.is('ios')) {
      this.navCtrl.push('LivePage', {}, { animate: true, direction: 'back' });
    } else {
      this.navCtrl.push('LivePage')
    }
  }

  geofence() {
    this.navCtrl.push('GeofencePage');
  }
}
