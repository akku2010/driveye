import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { AppLanguages } from '../providers/app-languages';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { NetworkProvider } from '../providers/network/network';
import { MenuProvider } from '../providers/menu/menu';

import { GoogleMaps, Spherical } from '@ionic-native/google-maps';
import { PopoverPage } from '../pages/add-devices/add-devices';
import { IonBottomDrawerModule } from '../../node_modules/ion-bottom-drawer';
import { Push } from '@ionic-native/push';
import { UpdateGroup } from '../pages/groups/update-group/update-group';
import { FilterPage } from '../pages/all-notifications/filter/filter';

// Custom components
import { SideMenuContentComponent } from '../../shared/side-menu-content/side-menu-content.component';
import { Geolocation } from '@ionic-native/geolocation';
import { ServiceProviderPage, UpdatePasswordPage } from '../pages/profile/profile';
import { AppVersion } from '@ionic-native/app-version';
import { SocialSharing } from '@ionic-native/social-sharing';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { CallNumber } from '@ionic-native/call-number';
// import { Pro } from '@ionic/pro';
// import { Injectable, Injector } from '@angular/core';

import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { PoiPage } from '../pages/live-single-device/live-single-device';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { ViewDoc } from '../pages/add-devices/upload-doc/view-doc/view-doc';
import { DocPopoverPage } from '../pages/add-devices/upload-doc/upload-doc';
import { ACDetailPage } from '../pages/ac-report/ac-report';
import { FuelEntryPage } from '../pages/fuel/fuel-consumption/fuel-consumption';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { ScreenOrientation } from "@ionic-native/screen-orientation";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LanguagesPage } from '../pages/login/login';
import { GeocoderProvider } from '../providers/geocoder/geocoder';
import { PagerProvider } from '../providers/pager/pager';
import { ModalPage } from '../pages/history-device/modal';
import { NotifModalPage } from '../pages/profile/settings/notif-setting/notif-modal';
// Import the plugin at the top (along with other imports)
import { NativeAudio } from '@ionic-native/native-audio';
import { ReportSettingModal } from '../pages/customers/modals/report-setting/report-setting';
import { FasttagPayNow } from '../pages/fastag-list/fastag/fastag';

// Pro.init('501b929f', {
//   appVersion: '20.1'
// })

// @Injectable()
// export class MyErrorHandler implements ErrorHandler {
//   ionicErrorHandler: IonicErrorHandler;

//   constructor(injector: Injector) {
//     try {
//       this.ionicErrorHandler = injector.get(IonicErrorHandler);
//     } catch (e) {
//       // Unable to get the IonicErrorHandler provider, ensure
//       // IonicErrorHandler has been added to the providers list below
//     }
//   }

//   handleError(err: any): void {
//     Pro.monitoring.handleNewError(err);
//     // Remove this if you want to disable Ionic's auto exception handling
//     // in development mode.
//     this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
//   }
// }

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    SideMenuContentComponent,
    PopoverPage,
    UpdateGroup,
    // OnCreate,
    FilterPage,
    ServiceProviderPage,
    UpdatePasswordPage,
    PoiPage,
    ViewDoc,
    DocPopoverPage,
    ACDetailPage,
    FuelEntryPage,
    LanguagesPage,
    ModalPage,
    NotifModalPage,
    ReportSettingModal,
    FasttagPayNow
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonBottomDrawerModule,
    SelectSearchableModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverPage,
    UpdateGroup,
    FilterPage,
    ServiceProviderPage,
    UpdatePasswordPage,
    PoiPage,
    ViewDoc,
    DocPopoverPage,
    ACDetailPage,
    FuelEntryPage,
    LanguagesPage,
    ModalPage,
    NotifModalPage,
    ReportSettingModal,
    FasttagPayNow
  ],
  providers: [
    StatusBar,
    SplashScreen,
    IonicErrorHandler,
    NativePageTransitions,
    // [{ provide: ErrorHandler, useClass: MyErrorHandler }],
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiServiceProvider,
    Network,
    NetworkProvider,
    MenuProvider,
    GoogleMaps,
    Geolocation,
    Spherical,
    Push,
    AppVersion,
    SocialSharing,
    TextToSpeech,
    Transfer,
    TransferObject,
    File,
    FileTransfer,
    FileTransferObject,
    Camera,
    FilePath,
    NativeGeocoder,
    CallNumber,
    ScreenOrientation,
    AppLanguages,
    GeocoderProvider,
    GeocoderProvider,
    PagerProvider,
    NativeAudio, // Add NativeAudio to providers
  ]
})
export class AppModule { }
